## Как добавить / обновить информацию

Для внесения изменений необходимо подать `merge-request` в `master` ветку, 
после апрува изменения войдут в паблик.

## Правила

* Имя файла должно иметь формат:

    * **UserName**.md
    * userid_**Number**.md

    Где:

    * **UserName** = username в Telegram

    * **Number**  = ID пользователя

* Формат файла [Markdown](https://core.telegram.org/bots/api#markdown-style)
